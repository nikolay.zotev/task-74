import "./App.css";

import { useMemo, useState } from "react";

function App() {
  const [text, setText] = useState("");

  const isNumber = useMemo(() => /^-?\d+$/.test(text), [text]);

  return (
    <div className="App">
      <div className="control has-icons-right">
        <input
          className="input is-large"
          type="text"
          placeholder="Enter number..."
          value={text}
          onChange={(e) => {
            setText(e.target.value);
          }}
        />
        <span className="icon is-small is-right">
          <i className={`fas fa-${isNumber ? "check" : "times"}`} />
        </span> 
      </div>
    </div>
  );
}

export default App;
